/*
 * DReal.h
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#ifndef DREAL_H
#define DREAL_H

#include <iostream>
#include "MyException.h"

typedef double Real;

class DReal {
public:
  DReal(Real v) : value(v), dvalue(0) { }
  DReal(Real v, Real dv) : value(v), dvalue(dv) { }
public:
  Real value;
  Real dvalue;
};

std::ostream & operator<<(std::ostream & o, const DReal & r);

DReal x_at(const Real v);
DReal operator+(const DReal & f, const DReal & g);
DReal operator-(const DReal & f, const DReal & g);
DReal operator*(const DReal & f, const DReal & g);


DReal operator/(const DReal & f, const DReal & g) throw (MyException);

DReal sin(const DReal & g);
DReal sqrt(const DReal & g);

auto sqr = [](const DReal & f) -> DReal{ return f*f; };
auto cube = [](const DReal & f) -> DReal { return f*f*f; };

#endif /* DREAL_H */
