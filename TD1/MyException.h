/*
 * MyException.h
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#ifndef MYEXCEPTION_H_
#define MYEXCEPTION_H_

struct MyException {
  const char * message;
};

#endif /* MYEXCEPTION_H_ */
