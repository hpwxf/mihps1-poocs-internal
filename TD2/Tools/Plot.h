/*
 * Plot.h
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#ifndef TOOLS_PLOT_H_
#define TOOLS_PLOT_H_

#include <functional>
#include <vector>
#include "Box.h"

void plot2d(const char * filename, const Box & arena, const int nx, const int ny, const std::function<Real(Real2)> & f);

void plot(const char * filename,
		const std::vector<Real2> & positions,
		const std::vector<Real> & times,
		const std::vector<Real> & speeds,
		const std::function<Real(Real2)> & f);

void plot(const char * filename, const Real xmin, const Real xmax, const int n, std::function<Real(Real)> f);

#endif /* TOOLS_PLOT_H_ */
