/*
 * Plot.cc
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#include <fstream>
#include <iostream>

#include "Tools/Plot.h"
#include "Tools/MyException.h"

void plot2d(const char * filename, const Box & arena, const int nx, const int ny, const std::function<Real(Real2)> & f) {
	const Real2 & origin = arena.origin;
	const Real2 & size = arena.size;
	const Real dx = (size.x)/(nx-1);
	const Real dy = (size.y)/(ny-1);

	const int nquadrangles = (nx-1) * (ny-1);
	const int npoints = nx * ny;

	std::cout << "Saving surface in '" << filename << "' with " << nquadrangles << " qudrangles\n";
	// VTK File formats : http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf

	std::ofstream file(filename);
	file << "# vtk DataFile Version 3.0\n"
			<< "Surface plot\n"
			<< "ASCII\n"
			<< "DATASET STRUCTURED_GRID\n"
			<< "DIMENSIONS " << nx << " " << ny << " " << 1 << "\n"
			<< "POINTS " << npoints << " float\n";

	for(int j=0;j<ny;++j)
		for(int i=0;i<nx;++i)
		{
			Real2 coord = origin + Real2{i*dx,j*dy};
			Real z = f(coord);
			file << (float)coord.x << " " << (float)coord.y << " " << (float)z << "\n";
		}

	file << "\nPOINT_DATA " << npoints << "\n"
			<< "SCALARS topography float 1\n"
			<< "LOOKUP_TABLE default\n";
	for(int j=0;j<ny;++j)
		for(int i=0;i<nx;++i)
		{
			Real2 coord = origin + Real2{i*dx,j*dy};
			Real z = f(coord);
			file << (float)z << "\n";
		}
	file << "\n";
}

void plot(const char * filename,
		const std::vector<Real2> & positions,
		const std::vector<Real> & times,
		const std::vector<Real> & speeds,
		const std::function<Real(Real2)> & f) {
	const int n = positions.size();

	std::cout << "Saving line in '" << filename << "' with " << n << " points\n";
	// VTK File formats : http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf

	std::ofstream file(filename);
	file << "# vtk DataFile Version 3.0\n"
			<< "Line plot\n"
			<< "ASCII\n"
			<< "DATASET UNSTRUCTURED_GRID\n\n"
			<< "POINTS " << n << " float\n";

	for(int i=0;i<n;++i) {
		Real2 coord = positions[i];
		Real z = f(coord);
		file << coord.x << " " << coord.y << " " << z << "\n";
	}

	file << "\nCELLS " << n-1 << " " << 3*(n-1) << "\n";
	for(int i=1;i<n;++i)
	{
		file << "2 " << i-1 << " " << i << "\n";
	}
	file << "\n";

	file << "\nCELL_TYPES " << n-1 << "\n";
	for(int i=1;i<n;++i)
	{
		file << "3\n";
	}
	file << "\n";

	file << "POINT_DATA " << n << "\n"
			<< "SCALARS topography float 1\n"
			<< "LOOKUP_TABLE default\n";
	for(int i=0;i<n;++i) {
		Real2 coord = positions[i];
		Real z = f(coord);
		file << z << "\n";
	}
	file << "\n";

	file << "SCALARS time float 1\n"
			<< "LOOKUP_TABLE default\n";
	for(int i=0;i<n;++i) {
		file << times[i] << "\n";
	}
	file << "\n";

	file << "CELL_DATA " << n-1 << "\n"
			<< "SCALARS speed float 1\n"
			<< "LOOKUP_TABLE default\n";
	for(int i=1;i<n;++i) {
		file << speeds[i-1] << "\n";
	}
	file << "\n";
}

void plot(const char * filename, const Real xmin, const Real xmax, const int n, std::function<Real(Real)> f) {
  std::ofstream out(filename);
  Real dx = (xmax-xmin)/n;
  Real x = xmin;
  for(int i=0;i<=n;++i,x+=dx) {
    try {
      Real fx = f(x);
      out << x << " " << fx << "\n";
    } catch (MyException & e) {
      std::cout << "Exception " << e.message << " at position x=" << x << "\n";
    }
  }
}
