/*
 * Diff.cc
 *
 *  Created on: Nov 16, 2016
 *      Author: havep
 */

#include "Tools/Numerical.h"
#include "Tools/MyException.h"

#include <iostream>

Real newton(Real x0, const Real eps, std::function<Real(Real)> f) {
	try {
		for(int i=0;i<100;++i) {
			Real fx = f(x0);
			Real dfx = diff(f,x0);
			// std::cout << "x0:" << x0 << " f(x0):" << fx << "\n";
			Real x1 = x0 - fx / dfx;
			if (std::abs(x1-x0)<eps) return x1;
			x0 = x1;
		}
		throw MyException{"Newton does not converge"};
	} catch (MyException & e) {
		std::cout << "Exception catched while solving newton:" << e.message << "\n";
		throw;
	}
}

auto diff(const std::function<Real(Real)> & f, const Real & x) -> Real {
    const Real epsilon = 1e-6;
	return (f(x+epsilon)-f(x))/epsilon;
}


auto integrate(const Real xmin, const Real xmax, const int n, std::function<Real(Real)> f)->Real {
  try {
	  const Real dx = (xmax-xmin)/n;
	  const Real fmin = f(xmin);
	  const Real fmax = f(xmax);
	  Real fmidcumul = 0.;
	  for(int i=1;i<n;++i) { fmidcumul += f(dx*i); }
	  return dx*((fmin+fmax)/2+fmidcumul);
  } catch (MyException & e)
  {
	  std::cout << "Exception catched while evaluating integration\n";
	  throw;
  }
}

