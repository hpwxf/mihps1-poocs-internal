/*
 * Surface.cc
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#include "Tools/Surface.h"
#include <cmath>

Surface::
Surface(const Box & arena, std::function<Real(Real2)> f)
: m_box(arena)
, m_f(f)
{
	;
}

const Box &
Surface::
box() const
{
	return m_box;
}

Real
Surface::
elevation(const Real2 & p) const
{
	return m_f(p);
}
