/*
 * Diff.h
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#ifndef TOOLS_NUMERICAL_H_
#define TOOLS_NUMERICAL_H_

#include <functional>

#include "Real.h"

Real newton(Real x0, const Real eps, std::function<Real(Real)> f);

auto diff(const std::function<Real(Real)> & f, const Real & x) -> Real;

auto integrate(const Real xmin, const Real xmax, const int n, std::function<Real(Real)> f)->Real;

inline auto sqr(const Real x)->Real { return x*x; }

#endif /* TOOLS_NUMERICAL_H_ */
