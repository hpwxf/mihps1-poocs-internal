/*
 * CSurface.h
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#ifndef TOOLS_SURFACE_H_
#define TOOLS_SURFACE_H_

#include <functional>

#include "Tools/Box.h"

//! Surface paramétrée par une fonction
class Surface {
public:
	//! Constructeur
	/*!
	 * @param arena zone où est définie la surface
	 * @param f une fonction de @a Real2 -> @a Real définissant la surface
	 */
	Surface(const Box & arena, std::function<Real(Real2)> f);

	//! Retourne la zone de définition de la surface
	const Box & box() const;

	//! Retourne l'élévation (en z) est un point de la zone de définition de la surface
	/*!
	 * @param p point où l'on cherche l'élévation (z)
	 * @return élévation au point de demandé
	 */
	Real elevation(const Real2 & p) const;
private:
	Box m_box;
	std::function<Real(Real2)> m_f;
};

#endif /* TOOLS_SURFACE_H_ */
