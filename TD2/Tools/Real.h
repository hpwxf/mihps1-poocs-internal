#ifndef REAL2_H
#define REAL2_H

#include <cmath>
#include <iostream>

typedef double Real;

//! Type de coordonnées 2D
struct Real2 {
  Real x;
  Real y;
};

inline Real2 operator-(const Real2 & a, const Real2 & b) { return {a.x-b.x, a.y-b.y}; }
inline Real2 operator+(const Real2 & a, const Real2 & b) { return {a.x+b.x, a.y+b.y}; }
inline Real2 & operator+=(Real2 & a, const Real2 & b) { a.x+=b.x; a.y+=b.y; return a; }
inline Real2 operator/(const Real2 & a, const Real & b) { return {a.x/b, a.y/b}; }
inline Real2 operator*(const Real & b, const Real2 & a) { return {a.x*b, a.y*b}; }
inline Real norm(const Real2 & a) { return std::sqrt(a.x*a.x+a.y*a.y); }

inline std::ostream & operator<<(std::ostream & o, const Real2 & p) { return o << '(' << p.x << ", " << p.y << ')'; }

/*
struct Real3 {
  Real2 p;
  Real z;
};

inline Real3 operator-(const Real3 & a, const Real3 & b) { return {a.p-b.p, a.z-b.z}; }
inline Real norm(const Real3 & a) { return std::sqrt(norm(a.p)*norm(a.p)+a.z*a.z); }
inline std::ostream & operator<<(std::ostream & o, const Real3 & p) { return o << '(' << p.p << ", " << p.z << ')'; }
*/

#endif /* REAL2_H */
