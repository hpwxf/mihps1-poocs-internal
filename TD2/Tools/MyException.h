/*
 * MyException.h
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#ifndef MYEXCEPTION_H_
#define MYEXCEPTION_H_

#include <string>

//! Classe d'exception
struct MyException {
	//! Message associé à l'exception
	std::string message;
};

#endif /* MYEXCEPTION_H_ */
