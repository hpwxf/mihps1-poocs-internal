/*
 * Array2.h
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#ifndef TOOLS_ARRAY2_H_
#define TOOLS_ARRAY2_H_

#include <vector>
#include <cassert>

template<typename T>
class Array2 {
public:
	Array2() : m_n1(0), m_n2(0) { }
	Array2(int n1, int n2) : m_n1(n1), m_n2(n2) {
		m_data.resize(n1*n2, T{});
	}
	Array2(int n1, int n2, const T t) : m_n1(n1), m_n2(n2) {
		m_data.resize(n1*n2, t);
	}
	virtual ~Array2() = default;

public:
	const T & operator()(int i, int j) const { assert(i >= 0 and i < m_n1 and j>=0 and j<m_n2); return m_data[m_n2 * i + j]; }
	decltype(std::vector<T>()[0]) operator()(int i, int j) { assert(i >= 0 and i < m_n1 and j>=0 and j<m_n2); return m_data[m_n2 * i + j]; }
	void fill(T t) { std::fill(m_data.begin(), m_data.end(), t); }

	void operator=(const Array2<T> & ) = delete;
	void operator=(const Array2<T> && v) {
		m_n1 = v.m_n1;
		m_n2 = v.m_n2;
		m_data = std::move(v.m_data);
	}

	int dim1() const { return m_n1; }
	int dim2() const { return m_n2; }
private:
	int m_n1, m_n2;
	std::vector<T> m_data;
};

#endif /* TOOLS_ARRAY2_H_ */
