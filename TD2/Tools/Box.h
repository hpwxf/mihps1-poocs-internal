/*
 * Arena.h
 *
 *  Created on: 6 nov. 2016
 *      Author: Pascal
 */

#ifndef TOOLS_BOX_H_
#define TOOLS_BOX_H_

#include "Tools/Real.h"

//! Boite 2D
struct Box {
	//! origine de la boite
	/*! Coin inférieur gauche; coordonnées minimales en x et y
	 */
	Real2 origin;

	//! Dimension de la boite
	/*! Réunie en une coordonnée de type @a Real2 symbolisant les coordonnées x et y b
	 */
	Real2 size;
};

#endif /* TOOLS_BOX_H_ */
