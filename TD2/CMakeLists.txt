cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)
project (Escape CXX)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# CMake Tutorial: 
# https://cmake.org/cmake-tutorial/

include_directories ("${PROJECT_SOURCE_DIR}")
add_subdirectory (Tools) 
add_subdirectory (RobotEngine)
add_subdirectory (Robots)

# add the executable
add_executable(Escape main.cc)
target_link_libraries (Escape Tools RobotEngine Robots)

add_executable(Escape2 main2.cc)
target_link_libraries (Escape2 Tools RobotEngine Robots)
