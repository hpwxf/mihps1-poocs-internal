#ifndef ROBOT11_H
#define ROBOT11_H

#include "RobotEngine/IRobot.h"

//! Politique 'simple' d'un robot
/*! Celui-ci se déplacera en ligne droite vers la cible par petites avancées de 1m avec un point intermédiaire (hors ligne droite) */
class Robot11 : public IRobot {
public:
  Robot11(const RobotParameter & param, const Real2 via) : m_param(param), m_via(via) { }
  virtual ~Robot11() = default;
public:
  void initialize(std::shared_ptr<const Surface> surface, const Real2 target) override;
  Real2 move(Real2 current_location) override;
  const RobotParameter & parameter() const override { return m_param; }

private:
  const RobotParameter m_param;
  Real2 m_via;
  std::shared_ptr<const Surface> m_surface;
  Real2 m_target;
};

#endif /* ROBOT11_H */
