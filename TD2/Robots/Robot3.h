#ifndef ROBOT3_H
#define ROBOT3_H

#include <memory>
#include "RobotEngine/IRobot.h"
#include "RobotEngine/IPhysics.h"
#include "Tools/Array2.h"

//! Politique 'explorateur' d'un robot
/*! Celui-ci remonte le point d'arrivée pour construire une carte de delai de proche en proche */
class Robot3 : public IRobot {
public:
  Robot3(const RobotParameter & param, std::shared_ptr<IPhysics> physics, const Real delta);
  virtual ~Robot3() = default;
public:
  void initialize(std::shared_ptr<const Surface> surface, const Real2 target) override;
  Real2 move(Real2 current_location) override;
  const RobotParameter & parameter() const override { return m_param; }

private:
  std::function<Real(Real2)> delay(const Real2 target) const;
  void plotDelays(const char * filename) const;

private:
  const RobotParameter m_param;
  std::shared_ptr<IPhysics> m_physics;
  std::shared_ptr<const Surface> m_surface;
  Real2 m_target;
  Array2<Real> m_delays;
  const Real m_delta;
};

#endif /* ROBOT3_H */
