#include "Robots/Robot3.h"
#include "Tools/Numerical.h"
#include <iostream>
#include <fstream>
#include <limits>
#include <stack>
#include <utility>

Robot3::
Robot3(const RobotParameter & param, std::shared_ptr<IPhysics> physics, const Real delta)
: m_param(param)
, m_delta(delta)
{
	m_physics = physics;
}

void
Robot3::
initialize(std::shared_ptr<const Surface> surface, const Real2 target)
{
	m_surface = surface;
	m_target = target;

	const Box & box = m_surface->box();

	const int n1 =  std::ceil(box.size.x/m_delta); // round up
	const int n2 =  std::ceil(box.size.y/m_delta); // round up
	m_delays = Array2<Real>(n1,n2, std::numeric_limits<Real>::quiet_NaN());

	const Real dx = box.size.x / (n1-1);
	const Real dy = box.size.y / (n2-1);
	std::cout << "dx=" << dx << " dy=" << dy << std::endl;

	auto lin = [n1,n2](int i, int j) -> int { return i*n2+j; };
	auto lin3 = [](int ik, int jk) -> int { return (ik+1)*3+(jk+1); };
	auto toReal2 = [dx,dy](const std::tuple<int,int> & pk) -> Real2 { return { std::get<0>(pk)*dx, std::get<1>(pk)*dy }; };
	Array2<Real> neighbor_distance(n1*n2, 9, std::numeric_limits<Real>::quiet_NaN());

	for(int i=0;i<n1;++i)
		for(int j=0;j<n2;++j)
			{
				Real2 p = box.origin + Real2{i*dx,j*dy};
				for(int ik=-1;ik<=+1;++ik)
					for(int jk=-1;jk<=+1;++jk)
						if ((ik!=0 or jk!=0)
								and i+ik>=0 and i+ik < n1
								and j+jk>=0 and j+jk < n2)
						{
							const Real2 pk = p + Real2{ik*dx,jk*dy};
							const Real dpkp = delay(p)(pk); // on évalue pk->p pour les besoins de l'algo
							neighbor_distance(lin(i,j), lin3(ik,jk)) = dpkp; // ie délai sur les arcs rentrant et non sortant
						}
			}

	Array2<bool> aggregated(n1,n2,false); // défini l'ensemble du sous-graphe déjà traité

	{
		const int target_i = (m_target.x-box.origin.x) / dx;
		const int target_j = (m_target.y-box.origin.y) / dy;
		const Real2 target_xy = box.origin + Real2{target_i*dx,target_j*dy};
		std::cout << "p=" << target_xy << " " << target_i << " " << target_j << std::endl;
		m_delays(target_i, target_j) = delay(m_target)(target_xy);
		aggregated(target_i, target_j) = true;
	}

	while(1)
	{ // Parcours des voisins du sous-graphe et recheche du plus proche (en délai)
		Real best_delay = std::numeric_limits<Real>::infinity();
		std::tuple<int,int> best_point;
		std::tuple<int,int> best_point_from;
		for(int i=0;i<n1;++i)
			for(int j=0;j<n2;++j)
				if (aggregated(i,j))
				{
					// std::cout << "Scanning around " << i << " " << j << std::endl;
					for(int ik=-1;ik<=+1;++ik)
						for(int jk=-1;jk<=+1;++jk)
							if ((ik!=0 or jk!=0)
									and i+ik>=0 and i+ik < n1
									and j+jk>=0 and j+jk < n2
									and !aggregated(i+ik, j+jk))
							{
								Real current_delay = neighbor_distance(lin(i,j), lin3(ik,jk));
								current_delay += m_delays(i,j);
								// std::cout << "Testing " << i+ik << " " << j+jk << " : " << current_delay << std::endl;
								if (current_delay < best_delay)
								{
									best_delay = current_delay;
									best_point = std::make_tuple(i+ik,j+jk);
									best_point_from = std::make_tuple(i,j);
								}
							}
				}
		if (best_delay != std::numeric_limits<Real>::infinity())
		{
			aggregated(std::get<0>(best_point), std::get<1>(best_point)) = true;
			m_delays(std::get<0>(best_point), std::get<1>(best_point)) = best_delay;
			// std::cout << "best:" << best_delay << " " << std::get<0>(best_point) << "," << std::get<1>(best_point) << " " << m_delays(std::get<0>(best_point), std::get<1>(best_point)) << std::endl;
		}
		else
		{
			// std::cout << "Not found : exiting" << std::endl;
			break;
		}
	}

	plotDelays("delays.vtk");
}

std::function<Real(Real2)>
Robot3::
delay(const Real2 target) const
{
	auto vitesse = m_physics->speedByAngle(parameter());

	return [surface=*m_surface,vitesse,target](Real2 p)->Real {
		const Real floor_distance = norm(target-p);
		const Real2 direction = (target-p)/floor_distance;
		const int ndiscrit = 100;

		auto altitude = [surface, p, direction](Real x)->Real { return surface.elevation(p + x * direction); };
		auto pente = [altitude](Real x)->Real { return std::atan(diff(altitude,x)); };
		auto path = [altitude](Real x)->Real { return std::sqrt(sqr(diff(altitude,x))+1); };
		const Real delay = integrate(0,floor_distance,ndiscrit, [pente,path,vitesse](Real x)->Real { return path(x)/vitesse(pente(x)); });
		return delay;
	};
}

Real2
Robot3::
move(Real2 current_location)
{
	const Real2 & p0 = current_location;

	// std::cout << "p0=" << p0 << " target=" << m_target << std::endl;


	if (norm(p0-m_target) < m_delta)
		return m_target;

	const Box & box = m_surface->box();
	const int n1 = m_delays.dim1();
	const int n2 = m_delays.dim2();
	const Real dx = box.size.x / (n1-1);
	const Real dy = box.size.y / (n2-1);
	const int p0_i = std::round((p0.x-box.origin.x) / dx); // round pour éviter le cumul d'erreur numérique
	const int p0_j = std::round((p0.y-box.origin.y) / dy);
	const Real2 p0_ = box.origin + Real2{p0_i*dx, p0_j*dy};
	Real dp0 = 0;
	if (norm(p0_-p0)>1e-3)
		dp0 = delay(p0_)(p0);
	// std::cout << "p0ij = " << p0_i << " " << p0_j << " " << m_delays(p0_i, p0_j)+dp0 << std::endl;

	Real2 best_target = {std::numeric_limits<Real>::quiet_NaN(), std::numeric_limits<Real>::quiet_NaN()};
	Real best_delay = std::numeric_limits<Real>::infinity();

	for(int ik=-1;ik<=1;++ik)
		for(int jk=-1;jk<=1;++jk)
			if ((ik!=0 or jk!=0)
					and p0_i+ik>=0 and p0_i+ik < n1
					and p0_j+jk>=0 and p0_j+jk < n2)
			{
				const Real2 pk = box.origin + Real2{(p0_i+ik)*dx, (p0_j+jk)*dy};
				Real d = m_delays(p0_i+ik, p0_j+jk);
				Real dp0_pk = delay(pk)(p0_);
				d += dp0_pk + dp0;
				if (d < best_delay)
				{
					best_delay = d;
					best_target = pk;
				}
			}
	assert(best_delay < std::numeric_limits<Real>::infinity());
	return best_target;
}

void
Robot3::
plotDelays(const char * filename) const
{
	const Box & arena = m_surface->box();
	const int nx = m_delays.dim1();
	const int ny = m_delays.dim2();

	const Real2 & origin = arena.origin;
	const Real2 & size = arena.size;
	const Real dx = (size.x)/(nx-1);
	const Real dy = (size.y)/(ny-1);

	const int nquadrangles = (nx-1) * (ny-1);
	const int npoints = nx * ny;

	std::cout << "Saving surface in '" << filename << "' with " << nquadrangles << " qudrangles\n";
	// VTK File formats : http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf

	std::ofstream file(filename);
	file << "# vtk DataFile Version 3.0\n"
			<< "Surface plot\n"
			<< "ASCII\n"
			<< "DATASET STRUCTURED_GRID\n"
			<< "DIMENSIONS " << nx << " " << ny << " " << 1 << "\n"
			<< "POINTS " << npoints << " float\n";

	for(int j=0;j<ny;++j)
		for(int i=0;i<nx;++i)
		{
			Real2 coord = origin + Real2{i*dx,j*dy};
			Real z = m_surface->elevation(coord);
			file << (float)coord.x << " " << (float)coord.y << " " << (float)z << "\n";
		}

	file << "\nPOINT_DATA " << npoints << "\n"
			<< "SCALARS delays float 1\n"
			<< "LOOKUP_TABLE default\n";
	for(int j=0;j<ny;++j)
		for(int i=0;i<nx;++i)
		{
			Real2 coord = origin + Real2{i*dx,j*dy};
			Real z = m_delays(i,j);
			file << (float)z << "\n";
		}
	file << "\n";
}


