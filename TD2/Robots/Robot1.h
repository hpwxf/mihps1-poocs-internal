#ifndef ROBOT1_H
#define ROBOT1_H

#include "RobotEngine/IRobot.h"

//! Politique 'simple' d'un robot
/*! Celui-ci se déplacera en ligne droite vers la cible par petites avancées de 1m
 * basé sur un gradient numérique de la distance (et non du délai) */
class Robot1 : public IRobot {
public:
	//! Constructeur du Robot1 à partir de ces paramètres physiques
	Robot1(const RobotParameter & param) : m_param(param) { }
	//! Destructeur par défaut
	virtual ~Robot1() = default;
public:
	void initialize(std::shared_ptr<const Surface> surface, const Real2 target) override;
	Real2 move(Real2 current_location) override;
	const RobotParameter & parameter() const override { return m_param; }

private:
	const RobotParameter m_param;
	std::shared_ptr<const Surface> m_surface;
	Real2 m_target;
};

#endif /* ROBOT1_H */
