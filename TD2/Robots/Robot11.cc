#include "Robots/Robot11.h"

#include "Tools/Numerical.h"
#include <iostream>

void
Robot11::
initialize(std::shared_ptr<const Surface> surface, const Real2 target)
{
	m_surface = surface;
	m_target = target;
}

Real2
Robot11::
move(Real2 current_location)
{
  const Real2 & p0 = current_location;
  if (norm(p0-m_via) < 0.1)
	  m_via = m_target;

  auto distance_to_target = [target=m_via](Real2 p)->Real { return norm(p-target); };
  auto f = distance_to_target;

  const Real eps = 1e-6;
  const Real dfx = (f(p0+Real2{eps,0}) - f(p0))/eps;
  const Real dfy = (f(p0+Real2{0,eps}) - f(p0))/eps;
  const Real2 df = Real2{dfx,dfy} / norm(Real2{dfx,dfy});

  Real d = distance_to_target(p0);
  if (d > 1) d = 1;

  return current_location - d * df;
}
