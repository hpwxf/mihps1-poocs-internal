/*
 * Physics.cpp
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#include "RobotEngine/LinearPhysics.h"
#include "Tools/MyException.h"

std::function<Real(Real)>
LinearPhysics::
speedByAngle(const RobotParameter & parameter) const
{
	// Mod��le de frottement sans effet significatif de l'air
	const Real frottement = parameter.power/parameter.maxSpeed; // _N
	return [puissance=parameter.power,
			frottement, poids=parameter.mass * gravity(),
			maxSpeed=parameter.maxSpeed]
			(Real angle)->Real {
		const Real speed = puissance/(frottement + poids * sin(angle));
		if (speed < 0)
			throw MyException{"Angle to hard: cannot go ahead"};

		return speed;
		// return std::min(speed, maxSpeed);
	};
}

