/*
 * Physics.h
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#ifndef ROBOTENGINE_LINEARPHYSICS_H_
#define ROBOTENGINE_LINEARPHYSICS_H_

#include <RobotEngine/IPhysics.h>

//! Implémentation la physique standard avec le modèle de frottement constant
/*! La gravité par défaut vaudra ici 10 m/s2 */
class LinearPhysics : public IPhysics {
public:
	LinearPhysics() = default;
	virtual ~LinearPhysics() = default;
public:
	std::function<Real(Real)> speedByAngle(const RobotParameter & parameter) const override;
	Real gravity() const override { return 10; }
};

#endif /* ROBOTENGINE_LINEARPHYSICS_H_ */
