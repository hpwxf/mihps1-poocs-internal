/*
 * Physics.cpp
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#include "RobotEngine/NonLinearPhysics.h"
#include "Tools/Numerical.h"
#include "Tools/MyException.h"

std::function<Real(Real)>
NonLinearPhysics::
speedByAngle(const RobotParameter & parameter) const
{
	// Mod��le avec effet significatif avec l'air
	auto frottement = [](Real v)->Real { return 1000 + 0.8 * v * v; };
	auto vitesse = [puissance=parameter.power,
					frottement, poids=parameter.mass * gravity(),
					vmax=parameter.maxSpeed]
					(Real angle)->Real {
		// ATTN : minimums locaux possibles suivant la fonction de frottement (par exemple si coef = 225)
		auto f = [puissance, frottement, poids, vmax, angle](Real x)->Real { return puissance - x*(frottement(x) + poids * sin(angle)); };
		const Real vguest = puissance/(frottement(0) + poids * sin(angle));
		const Real v = newton(vguest, 1e-4, f);
		if (v < 0)
			throw MyException{"Angle to hard: cannot go ahead"};
		return v;
	};
	return vitesse;
}
