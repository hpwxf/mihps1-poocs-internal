/*
 * RobotParameter.h
 *
 *  Created on: 19 nov. 2016
 *      Author: Pascal
 */

#ifndef ROBOTENGINE_ROBOTPARAMETER_H_
#define ROBOTENGINE_ROBOTPARAMETER_H_

//! Paramètres physiques d'un @a IRobot
struct RobotParameter {
	//! power (_W)
	Real power;
	//! weight (_kg)
	Real mass;
	//! max speed (_m/s)
	Real maxSpeed;
};

#endif /* ROBOTENGINE_ROBOTPARAMETER_H_ */
