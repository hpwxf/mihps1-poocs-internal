#ifndef ROBOT_CONTROLLER_H
#define ROBOT_CONTROLLER_H

#include <memory>
#include <vector>
#include "RobotEngine/IRobot.h"
#include "RobotEngine/IPhysics.h"

//! Controleur de robot qui appel, valide et applique les directives de déplacemente d'un @a IRobot
class RobotController {
public:
	//! Constructeur du RobotController
	/*!
	 * @param physics la physique de la simulation
	 * @param r un Robot à gérer
	 */
	RobotController(const std::shared_ptr<IPhysics> & physics, std::unique_ptr<IRobot> && r);
	//! Destructeur par défaut
	virtual ~RobotController() = default;
public:
	//! Lance une course sur une surface donnée, un point de départ et une cible
	/*!
	 * Cette action calcule les positions et temps résultants de cette "course".
	 * Ces résultats seront disponibles via les méthodes @a positions et @a times.
	 *
	 * @param surface la surface pour la course
	 * @param start_point le point de départ
	 * @param target le point d'arrivée (cible)
	 */
	void run(const std::shared_ptr<Surface> surface, const Real2 start_point, const Real2 target);

	//! Retourne les positions intermédaires du robot lors de la précédent course
	/*! Défini suite à l'appel de @a run */
	const std::vector<Real2> positions() const { return m_positions; }
	//! Retourne les temps intermédaires du robot lors de la précédent course
	/*! Défini suite à l'appel de @a run */
	const std::vector<Real> times() const { return m_times; }

private:
	std::shared_ptr<IPhysics> m_physics;
	std::unique_ptr<IRobot> m_robot;
	std::shared_ptr<Surface> m_surface;
	Real2 m_start_point;
	Real2 m_target;

	std::vector<Real2> m_positions;
	std::vector<Real> m_times;
};

#endif /* ROBOT_CONTROLLER_H */
