#include "RobotEngine/RobotController.h"
#include "Tools/Numerical.h"
#include "Tools/MyException.h"
#include <iostream>

RobotController::
RobotController(const std::shared_ptr<IPhysics> & physics, std::unique_ptr<IRobot> && r)
{
	m_physics = physics;
	m_robot = std::move(r);
}

void
RobotController::
run(const std::shared_ptr<Surface> surface, const Real2 start_point, const Real2 target)
{
	m_surface = surface;
	m_start_point = start_point;
	m_target = target;

	m_robot->initialize(m_surface, m_target);

	// Numerical parameters
	const int ndiscrit = 100;
	const Real target_size = 1;

	// Position and time evolution
	Real2 current_point = m_start_point;
	Real current_time = 0;

	m_positions = {current_point};
	m_times = {current_time};

	auto vitesse = m_physics->speedByAngle(m_robot->parameter());
	auto distance_to_target = [target=m_target](Real2 p)->Real { return norm(p-target); };

	while(distance_to_target(current_point) > target_size)
	{
		try {
			std::cout << "#" << m_times.size()-1 << " @" << current_time << " : position = " << current_point << " ; distance to target = " << distance_to_target(current_point) << std::endl;

			const Real2 target_point = m_robot->move(current_point);
			// std::cout << "Robot displacement : " << target_point-current_point << std::endl;

			const Real floor_distance = norm(target_point-current_point);
			const Real2 direction = (target_point-current_point)/floor_distance;

			auto altitude = [surface=*m_surface, current_point, direction](Real x)->Real { return surface.elevation(current_point + x * direction); };
			auto pente = [altitude](Real x)->Real { return std::atan(diff(altitude,x)); };
			auto path = [altitude](Real x)->Real { return std::sqrt(sqr(diff(altitude,x))+1); };
			const Real delay = integrate(0,floor_distance,ndiscrit, [pente,path,vitesse](Real x)->Real { return path(x)/vitesse(pente(x)); });

			current_point = target_point;
			current_time += delay;
			m_positions.push_back(current_point);
			m_times.push_back(current_time);
		} catch (MyException & e)
		{
			std::cerr << "Catch exception:" << e.message << "\n";
			std::cerr << "Aborting run\n";
			return;
		}
	}
	// std::cout << "#" << m_times.size()-1 << " @" << current_time << " : position = " << current_point << " ; distance to target = " << distance_to_target(current_point) << std::endl;
}
