/*
 * IPhysics.h
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#ifndef ROBOTENGINE_IPHYSICS_H_
#define ROBOTENGINE_IPHYSICS_H_

#include <functional>

#include "Tools/Real.h"
#include "RobotEngine/RobotParameter.h"

//! Modèle physique
class IPhysics {
public:
	//! Fonction retournant la vitesse d'un robot en fonction des paramètres physiques
	/*!
	 * @param parameter paramètre physique du robot
	 * @return fonction calculant la vitesse du robot à partir d'une pente
	 * @exception retourne une exception de type @a MyException si la configuration est invalide@a current_location
	 */
	virtual std::function<Real(Real)> speedByAngle(const RobotParameter & parameter) const = 0;
	//! Constante d'accélération de gravité (_m/s^2)
	virtual Real gravity() const = 0;
};

#endif /* ROBOTENGINE_IPHYSICS_H_ */
