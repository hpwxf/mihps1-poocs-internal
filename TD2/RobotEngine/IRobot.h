#ifndef IROBOT_H
#define IROBOT_H

#include "Tools/Real.h"
#include "Tools/Surface.h"
#include "RobotEngine/RobotParameter.h"
#include <memory>

//! Système de décision de déplacement d'un robot
/*! Il ne peut que "lire" son environnement et la traduction ou à la validation physique
 * de ses actions sont à la charge de @a RobotControler et @a IPhysics
 */
class IRobot {
public:
  virtual ~IRobot() { }
public:
  //! Associe une surface surface au robot pour son lancement et lui donne sa cible
  /*! @param surface la surface associée
   *  @param target la cible à atteindre
   */
  virtual void initialize(std::shared_ptr<const Surface> surface, const Real2 target) = 0;
  //! Calcul un déplacement en ligne droite en partant de la position donnée
  /*!
   * @param current_location la position de départ
   * @return la position intermédiare à atteindre en ligne droite
   */
  virtual Real2 move(Real2 current_location) = 0;
  //! Retourne les paramètres physiques du robot
  virtual const RobotParameter & parameter() const = 0;
};

#endif /* IROBOT_H */
