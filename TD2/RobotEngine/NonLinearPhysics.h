/*
 * Physics.h
 *
 *  Created on: Nov 18, 2016
 *      Author: havep
 */

#ifndef ROBOTENGINE_NONLINEARPHYSICS_H_
#define ROBOTENGINE_NONLINEARPHYSICS_H_

#include <RobotEngine/IPhysics.h>

class NonLinearPhysics : public IPhysics {
public:
	NonLinearPhysics() = default;
	virtual ~NonLinearPhysics() = default;
public:
	std::function<Real(Real)> speedByAngle(const RobotParameter & parameter) const override;
	Real gravity() const override { return 10; }
};

#endif /* ROBOTENGINE_NONLINEARPHYSICS_H_ */
