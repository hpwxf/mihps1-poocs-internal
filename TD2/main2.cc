#include <iostream>
#include <memory>

#include "RobotEngine/RobotController.h"
#include "RobotEngine/IRobot.h"

#include "RobotEngine/IPhysics.h"
#include "RobotEngine/LinearPhysics.h"
#include "RobotEngine/NonLinearPhysics.h"

#include "Robots/Robot1.h"
#include "Robots/Robot3.h"
#include "Robots/Robot11.h"

#include "Tools/Surface.h"
#include "Tools/Plot.h"
#include "Tools/Box.h"

#include <vector>
#include <cassert>
#include "Tools/Real.h"

int main(int argc, char ** argv) {
	auto sqr = [](const Real & f) -> Real{ return f*f; };

#if 1
	auto f = [sqr](Real2 p)->Real {
		const Real xsmooth = 375;
		const Real dxsmooth = 25;
		const Real dysmooth = 25;
		Real fx = std::sin(sqr(p.x/100))*p.x/10;
		if (p.x >= xsmooth)
			fx *= std::exp(-sqr((p.x-xsmooth)/dxsmooth));
		const Real dy = std::abs(p.y);
		if (dy<dysmooth) {
			return fx;
		} else {
			Real fy = std::exp(-sqr((dy-dysmooth)/dysmooth));
			return fx*fy;
		}
	};

	std::shared_ptr<Surface> surface = std::make_shared<Surface>(Box{Real2{0,-150}, Real2{500,300}}, f);
	// std::shared_ptr<Surfacea> surface = std::make_shared<Surface>(Box{Real2{0,-150}, Real2{300,300}}, f);
#else
	// auto f = [](Real2 p)->Real { return (p.x - std::floor(p.x / 10) * 10); };
	auto f = [](Real2 p)->Real { return p.x; };
	std::shared_ptr<Surface> surface = std::make_shared<Surface>(Box{Real2{0, -50}, Real2{100,100}}, f);
#endif
	auto surface_function = [surface](const Real2 & p)->Real { return surface->elevation(p); };
	plot2d("surface.vtk", surface->box(), 100, 100, surface_function );

	const Real2 start_point { surface->box().origin.x,
							  surface->box().origin.y + surface->box().size.y / 2};
	const Real2 target { surface->box().origin.x + surface->box().size.x,
						 surface->box().origin.y + surface->box().size.y / 2};
/*
	{
		const Real2 direction{1,0};
		const Real xmin = 0;
		const Real xmax = 300;
		auto altitude = [surface, current_point, direction](Real x)->Real { return surface.elevation(current_point + x * direction); };
		auto pente = [altitude](Real x)->Real { return std::atan(diff(altitude,x)); };
		plot("altitude.gp",xmin,xmax,100,altitude);
		plot("pente.gp",xmin,xmax,100,[pente](Real x)->Real { return pente(x); });
	    plot("vitesse.gp",xmin,xmax,100,[pente,vitesse](Real x)->Real { return vitesse(pente(x)); });
	}
*/

	std::shared_ptr<IPhysics> physics(new LinearPhysics());

	const RobotParameter robot_parameter{ 10000/* _W */, 100/* _kg */, 10/* _m/s */ };

	if (argc == 1)
		std::cout << "Usage: " << argv[0] << " [RobotName]" << std::endl;

	for(int iargc=1;iargc<argc;++iargc) {
		std::unique_ptr<IRobot> robot;
		if (argv[iargc] == std::string("Robot1"))
			robot.reset(new Robot1{robot_parameter});
		else if (argv[iargc] == std::string("Robot11"))
			robot.reset(new Robot11(robot_parameter, surface->box().origin + Real2{surface->box().size.x/2,0}));
		else if (argv[iargc] == std::string("Robot3"))
			robot.reset(new Robot3{robot_parameter, physics, 10});
		else
		{
			std::cerr << "Undefined Robot" << std::endl;
			exit(1);
		}

		RobotController rc(physics, std::move(robot));

		rc.run(surface, start_point, target);

		const std::vector<Real2> & positions = rc.positions();
		const std::vector<Real> & times = rc.times();

		Real current_distance = 0;
		Real current_flying_distance = 0;
		std::vector<Real> distances = { current_distance };
		std::vector<Real> speeds;

		assert(positions.size() == times.size());

		const int ndiscrit = 100;

		for(int i=1;i<positions.size();++i)
		{
			const Real2 pa = positions[i-1];
			const Real2 pb = positions[i];

			const Real flying_distance = norm(pb-pa);
			const Real2 direction=(pb-pa)/norm(pb-pa);
			auto altitude = [surface, current_point=pa, direction](Real x)->Real {
				return surface->elevation(current_point + x * direction);
			};

			Real distance = 0;
			const Real dx = flying_distance / ndiscrit;
			for(int j=0;j<ndiscrit;++j) {
				const Real x1 = (j+1) * dx;
				const Real x0 = j*dx;
				distance += norm(Real2{x1, altitude(x1)} - Real2{x0, altitude(x0)});
			}

			const Real delay = times[i]-times[i-1];
			current_distance += distance;
			current_flying_distance += flying_distance;
			distances.push_back(current_distance);
			speeds.push_back(distance/delay);
			//std::cout << "Current step: " << i << " flying_distance=" << flying_distance << " distance=" << distances.back() << " speed=" << speeds.back() << " delay=" << delay << "\n";
		}
		std::cout << "Final time=" << times.back() << " over distance=" <<  current_distance << " (flying=" << current_flying_distance << ")\n";
		std::cout << "Mean speed=" << current_distance/times.back() << "\n";


		std::string filename = "chemin-";
		filename += argv[iargc];
		filename += "-plot.vtk";

		plot(filename.c_str(), positions, times, speeds, surface_function);
	}
	return 0;
}
